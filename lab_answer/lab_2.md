1. Perbedaan JSON dan XML
Terdapat beberapa perbedaan antara JSON dan XML:
- JSON hanyalah format dalam JavaScript, sedangkan XML merupakan bahasa markup yang memiliki tag untuk mendefinisikan elemen
- JSON mendukung array, sedangkan XML tidak mendukung array secara langsung(setiap objek harus diberi tag)
- JSON menyimpan data seperti prinsip map, yaitu ada key dan valuenya. Sedangkan XML, data disimpan sebagai tree structure

2. Perbedaan HTML dan XML
Terdapat beberapa perbedaan antara HTML dan XML:
- HTML lebih menitikberatkan pada penampilan data, sedangkan XML pada transfer data
- HTML tidak menyediakan dukungan namespaces, sedangkan XML menyediakan dukungan namespaces
- Pada HTML tag nya sudah ditentukan dari awal, sedangkan pada XML tag nya tidak ditentukan dari awal dan dapat dikembangkan
